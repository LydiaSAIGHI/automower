/**
 * 
 */
package fr.companyx.automower.commons;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import fr.companyx.automower.exception.IllegalCommandFormatException;
import fr.companyx.automower.model.Command;

/**
 * @author lydiasaighi
 *
 */
public abstract class CommandUtils {

	private static final String COMMANDS_FORMAT = "[LRF]*";

	private CommandUtils() {

	}

	public static List<Command> getCommands(String commands) throws IllegalCommandFormatException {
		isValidCommands(commands);
		return Arrays.asList(commands.split("")).stream().map(Command::getCommand).collect(Collectors.toList());
	}

	public static void isValidCommands(String commands) throws IllegalCommandFormatException {

		if (!Pattern.matches(COMMANDS_FORMAT, commands)) {
			throw new IllegalCommandFormatException(MessagesErrors.INVALID_COMMAND_FORMAT + commands);
		}

	}
}
