/**
 * 
 */
package fr.companyx.automower.commons;

import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * @author lydiasaighi
 *
 */
public class FileReader {

	
	
	private FileReader() {
		
	}

	public static  List<String> readAllLines(String filePath) throws IOException {
		List<String> lines = new ArrayList<>();

		try (Stream<String> stream = Files.lines(Paths.get(filePath))) {

			lines = stream.collect(toList());

		} catch (NoSuchFileException e) {
			throw new  NoSuchFileException ("No such File : " + filePath);
		}
		return lines;
	}
}
