/**
 * 
 */
package fr.companyx.automower.exception;

/**
 * @author lydiasaighi
 *
 */
public class IllegalFileFormatException extends IllegalFormatException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public IllegalFileFormatException(String message) {
		super(message);
	}

}
