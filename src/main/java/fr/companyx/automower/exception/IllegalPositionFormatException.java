/**
 * 
 */
package fr.companyx.automower.exception;

/**
 * @author lydiasaighi
 *
 */
public class IllegalPositionFormatException extends IllegalFormatException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public IllegalPositionFormatException(String message) {
		super(message);
	}

}
