package fr.companyx.automower.model;

public enum Command {

	LEFT("L"), RIGHT("R"), FORWARD("F");

	private String code;

	private Command(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	/**
	 * Get command by code
	 * @param code
	 * @return
	 */
	public static Command getCommand(String code) {

		for (Command command : Command.values()) {
			if (command.getCode().equals(code)) {
				return command;
			}
		}
		return null;
	}
	
}
