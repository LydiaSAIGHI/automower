/**
 * 
 */
package fr.companyx.automower.model;

import java.util.List;

/**
 * @author lydiasaighi
 *
 */
public class Mower {

	private Position position;
	private List<Command> commands;

	public Mower(Position position, List<Command> commands) {
		super();
		this.position = position;
		this.commands = commands;
	}

	/**
	 * 
	 * @param start
	 *            executing instructions to mow the lawn
	 * @return : the final position of the mower
	 */
	public Position mowingLawn(Coordinates limitLown) {

		for (Command cmd : commands) {
			switch (cmd) {
			case LEFT :
				turnLeft();
				break;
			case RIGHT:
				turnRight();
				break;
			case FORWARD:
				moveForward(limitLown);
				break;
			}
		}
		return position;
	}

	/**
	 * Turn the mower at 90° on the left without moving the mower.
	 */
	private void turnLeft() {
		switch (position.getOrientation()) {
		case NORD:
			position.setOrientation(Orientation.WEST);
			break;

		case EST:
			position.setOrientation(Orientation.NORD);
			break;

		case SUD:
			position.setOrientation(Orientation.WEST);
			break;

		case WEST:
			position.setOrientation(Orientation.SUD);
			break;
		
		}

	}

	/**
	 * Turn the mower at 90° on the right without moving the mower.
	 */
	private void turnRight() {

		switch (position.getOrientation()) {

		case NORD:
			position.setOrientation(Orientation.EST);
			break;

		case EST:
			position.setOrientation(Orientation.SUD);
			break;

		case SUD:
			position.setOrientation(Orientation.EST);
			break;

		case WEST:
			position.setOrientation(Orientation.NORD);
			break;
		}
	}

	/**
	 * The mower will move forward from one space in the direction in which it faces
	 * and without changing the orientation. The position after moving must be
	 * inside the lawn
	 * 
	 * @param limitLown
	 */
	private void moveForward(Coordinates limitLown) {

		if (position.getOrientation().equals(Orientation.NORD)  && position.getY() < limitLown.getY()) {
			position.incrementY();
		}
		if (position.getOrientation().equals(Orientation.EST) && position.getX() < limitLown.getX()) {
			position.incrementX();
		}
		if (position.getOrientation().equals(Orientation.SUD) && position.getY() > 0) {
			position.decrementY();
		}
		if (position.getOrientation().equals(Orientation.WEST) && position.getX() > 0) {
			position.decrementX();
		}
	}
}
