/**
 * 
 */
package fr.companyx.automower.model;

/**
 * @author lydiasaighi
 *
 */
public class Position extends Coordinates {

	private Orientation orientation;

	public Position(Integer x, Integer y, Orientation orientation) {
		super(x, y);
		this.orientation = orientation;
	}

	public Orientation getOrientation() {
		return orientation;
	}

	public void setOrientation(Orientation orientation) {
		this.orientation = orientation;
	}

	public void incrementX() {
		x++;
	}

	public void decrementX() {
		x--;
	}

	public void incrementY() {
		y++;
	}

	public void decrementY() {
		y--;
	}

	@Override
	public String toString() {
		return x + " " + y + " " + orientation.getCode();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((orientation == null) ? 0 : orientation.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Position other = (Position) obj;

		return orientation == other.orientation;
	}

}
