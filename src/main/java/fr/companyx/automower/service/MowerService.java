/**
 * 
 */
package fr.companyx.automower.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fr.companyx.automower.commons.CommandUtils;
import fr.companyx.automower.commons.FileReader;
import fr.companyx.automower.commons.PositionUtils;
import fr.companyx.automower.exception.IllegalFileFormatException;
import fr.companyx.automower.exception.IllegalFormatException;
import fr.companyx.automower.model.Command;
import fr.companyx.automower.model.Coordinates;
import fr.companyx.automower.model.Mower;
import fr.companyx.automower.model.Position;

/**
 * @author lydiasaighi
 *
 */
public class MowerService {

	public List<Position> launch(String filePath) throws IllegalFormatException, IOException {

		List<String> lines = FileReader.readAllLines(filePath);

		if (lines.isEmpty()) {
			throw new IllegalFileFormatException("Empty file !");
		}

		Iterator<String> lineIterator = lines.iterator();
		Coordinates limitLawn = PositionUtils.getLimitOfLawn(lineIterator.next());
		List<Mower> mowers = getMowers(lineIterator);

		return start(mowers, limitLawn);
	}

	/**
	 * Construct mower objects
	 * 
	 * @param lineIterator
	 * @return
	 * @throws IllegalFormatException
	 */
	private List<Mower> getMowers(Iterator<String> lineIterator) throws IllegalFormatException {
		List<Mower> mowers = new ArrayList<>();
		while (lineIterator.hasNext()) {

			Position startPosition = PositionUtils.getStartPosition(lineIterator.next());
            
			//Get all instructions
			List<Command> commands = CommandUtils.getCommands(lineIterator.next());

			mowers.add(new Mower(startPosition, commands));
		}
		return mowers;
	}

	/**
	 * Each mower moves sequentially. When a mower has finished, it gives the final
	 * position (X Y Orientation). 
	 * 
	 * @param mowers
	 * @param limitLawn
	 * @return
	 */
	private List<Position> start(List<Mower> mowers, Coordinates limitLawn) {

		List<Position> positions = new ArrayList<>();
		mowers.stream().forEach(mower -> positions.add(mower.mowingLawn(limitLawn)));
		return positions;
	}

}
