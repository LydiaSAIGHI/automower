/**
 * 
 */
package fr.companyx.automower.commons;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.List;
import static java.util.stream.Collectors.toList;
import org.junit.Test;

import fr.companyx.automower.exception.IllegalCommandFormatException;
import fr.companyx.automower.model.Command;

/**
 * @author lydiasaighi
 *
 */
public class CommandUtilsTest {

	@Test
	public void testGetCommands() throws IllegalCommandFormatException {

		List<String> commands = Arrays.asList("L", "R", "F", "F", "R", "F", "F", "F", "R");

		List<String> result = CommandUtils.getCommands("LRFFRFFFR").stream().map(Command::getCode).collect(toList());

		assertEquals(commands, result);

	}

	/**
	 * Test if an exception is thrown when a command format is invalid
	 */
	@Test
	public void testIsValidCommands() {
		String command = "LRFFRFFFRA";
		try {
			CommandUtils.getCommands(command).stream().map(Command::getCode).collect(toList());
			fail("getCommands is successfully runned");
		} catch (IllegalCommandFormatException e) {
			assertTrue(MessagesErrors.INVALID_COMMAND_FORMAT.concat(command).equals(e.getMessage()));
		}

	}
}
