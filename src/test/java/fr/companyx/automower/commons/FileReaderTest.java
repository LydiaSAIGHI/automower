/**
 * 
 */
package fr.companyx.automower.commons;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * @author lydiasaighi
 *
 */
public class FileReaderTest {
	private static final String FILE_NAME = "TestFile.text";
	private String filePath;

	@Before
	public void rest() {
		ClassLoader classLoader = this.getClass().getClassLoader();
		filePath = new File(classLoader.getResource(FILE_NAME).getFile()).getPath();
	}

	@Test
	public void testReadFile() throws IOException {
		List<String> lines = Arrays.asList("5 5", "1 2 N", "LFLFLFLFF", "3 3 E", "FFRFFRFRRF");
		assertEquals(lines, FileReader.readAllLines(filePath));
	}

}
