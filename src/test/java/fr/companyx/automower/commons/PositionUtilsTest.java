/**
 * 
 */
package fr.companyx.automower.commons;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import fr.companyx.automower.exception.IllegalFormatException;
import fr.companyx.automower.exception.IllegalPositionFormatException;
import fr.companyx.automower.model.Coordinates;
import fr.companyx.automower.model.Orientation;
import fr.companyx.automower.model.Position;

/**
 * @author lydiasaighi
 *
 */
public class PositionUtilsTest {

	@Test
	public void testGetInitialPosition() throws IllegalFormatException {
		Position position = new Position(2, 3, Orientation.WEST);
		assertEquals(position, PositionUtils.getStartPosition("2 3 W"));
	}

	@Test
	public void testGetLimitOfLawn() throws IllegalFormatException {
		Coordinates coordinates = new Coordinates(6, 6);
		assertEquals(coordinates, PositionUtils.getLimitOfLawn("6 6"));
	}

	/**
	 * Test if an exception is thrown when the initial position format is not valid
	 */
	@Test
	public void testisPositionFormatNotValid() {
		String position = "2 4 T";
		try {

			PositionUtils.getStartPosition(position);
			fail("isValidPosition is successfully runned");
		} catch (IllegalPositionFormatException e) {
			assertTrue(MessagesErrors.INVALID_POSITION_FORMAT.concat(position).equals(e.getMessage()));
		}

	}

	/**
	 * Test if an exception is thrown when the cardinal coordinate format of the
	 * upper right corner is not invalid
	 */
	@Test
	public void testisLimitOfLawnNotValid() {
		String coordinate = "23";
		try {
			PositionUtils.getLimitOfLawn(coordinate);
			fail("isValidLimitLawn is successfully runned");
		} catch (IllegalPositionFormatException e) {
			assertTrue(MessagesErrors.INVALID_COORDINATE_FORMAT.concat(coordinate).equals(e.getMessage()));
		}

	}
}
