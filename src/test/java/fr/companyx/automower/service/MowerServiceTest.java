/**
 * 
 */
package fr.companyx.automower.service;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import fr.companyx.automower.exception.IllegalFormatException;
import fr.companyx.automower.model.Orientation;
import fr.companyx.automower.model.Position;

/**
 * @author lydiasaighi
 *
 */

public class MowerServiceTest {
	private static final String FILE_NAME = "File.text";
	private String filePath;

	@Before
	public void rest() {
		ClassLoader classLoader = this.getClass().getClassLoader();
		filePath = new File(classLoader.getResource(FILE_NAME).getFile()).getPath();
	}

	@Test
	public void testLaunch() throws IllegalFormatException, IOException {
		List<Position> positions = new ArrayList<>();
		Position position1 = new Position(0, 0, Orientation.SUD);
		Position position2 = new Position(5, 1, Orientation.EST);

		positions.add(position1);
		positions.add(position2);

		assertEquals(positions, new MowerService().launch(filePath));
	}
}
